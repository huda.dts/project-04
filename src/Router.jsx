import React from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import Login from './pages/Login'
import App from './App';

// import App from './App'

function Router(){
    return (
        <BrowserRouter>
            <Route exact path="/" component={Login}/>
            <Route exact path="/Home" component={App}/>
        </BrowserRouter>
    )
}

export default Router