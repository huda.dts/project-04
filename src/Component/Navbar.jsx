import React, { Component } from 'react'

export default class Navbar extends Component {
    render() {
        return (
        <div class="card-header">
          <ul class="nav nav-pills card-header-pills">
            <li class="nav-item">
              <a class="nav-link active" href="/">Manage</a>
            </li>
            <li class="nav-item" id="Todo">
            <button class="nav-link" href="/" type="click">To Do</button>
            </li>
            <li class="nav-item" id="Logout">
              <a class="nav-link disabled "  href="/" tabindex="-1" aria-disabled="true">Log Out</a>
            </li>
          </ul>
        </div>
        )
    }
}
