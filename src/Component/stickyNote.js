import React from 'react';
import TodoItems from './TodoItems';
import Navbar from './Navbar';

export default class StickyNote extends React.Component {

      constructor(props){
        super(props);

        this.state = {
            items: []
        };

        this.addItem = this.addItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
      }
      
      render() {
        return (
      <div class="card text-center">
        <Navbar />
        <div class="container">
        <div class="card-body">
          <h5 class="card-title">Sticky Note</h5>
          <p class="card-text">To do List Hari Ini</p>
          <form onSubmit={this.addItem}>
          <input  ref={(a) => this._inputElement = a}
                          placeholder="enter task">
                  </input>
                  <button type="submit">add</button>
              </form>
              <TodoItems entries={this.state.items}
                      delete={this.deleteItem}/>
          </div>
        </div>
      </div>
      
      )
  }
 
  addItem(e) {
    e.preventDefault();

    if(this._inputElement.value !== "") {
        var newItem = {
            text: this._inputElement.value,
            key: Date.now()
        };
    
        this.setState((prevState) => {
            return {
                items : prevState.items.concat(newItem)
            };
        });
    }

    this._inputElement.value = "";

    console.log(this.state.items);
  }

  deleteItem(key){
    var filteredItems = this.state.items.filter(function (item) {
        return (item.key !== key)
    });

  this.setState({
    items: filteredItems
    });
  }  
}
